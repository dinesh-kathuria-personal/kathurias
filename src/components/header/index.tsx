import { h } from 'preact';
import { Link } from 'preact-router/match';
import * as style from './style.css';

interface HeaderProps {
    isDefault: boolean;
}
const Header = (props: HeaderProps): JSX.Element => {
    return (
        <header className={style.header}>
            <h1>Preact App - {props ? 'Default' : 'Custom'}</h1>
            <nav>
                <Link activeClassName={style.active} href="/">
                    Home
                </Link>
                <Link activeClassName={style.active} href="/profile">
                    Me
                </Link>
                <Link activeClassName={style.active} href="/profile/john">
                    John
                </Link>
            </nav>
        </header>
    );
};

export default Header;
